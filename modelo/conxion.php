<?php
$puntos = isset($_POST['puntos']) ? $_POST['puntos'] : '../';
	require_once($puntos. 'config/config.php');
	/**
	 * Conexion con la bd
	 */
	class Conexion extends ConfigBd
	{
		private function ConexionBD () {
			$con = new PDO('mysql:host='.$this->host.'; dbname='.$this->bd, $this->usuario, $this->pass);
			return $con;
		}

		public function CargarDatos($sql, $parametros = null){
			$con = $this->ConexionBD();
			$datos = $con->prepare($sql);
			$datos->execute($parametros);
			$con = '';
			return $datos->fetchAll();
		}

		function Ejecutar($sql, $parametros = null){
			$con = $this->ConexionBD();
			$datos = $con->prepare($sql);
			$con = '';
			return $datos->execute($parametros);
		}
	}
 ?>