<?php
$_POST['puntos'] = '../../';
require($_POST['puntos']. "controlador/parametrosControlador.php");
$datos = Cargar();

 ?>
<div class="panel panel-primary">
	<div class="panel-heading"> Parámetros generales </div>
	<div class="panel-body">

		<form action="#" method="post" name="form" class="FormularioAjax form-horizontal" enctype="multipart/form-data" role="form">
			<input type="hidden" name="puntos" value="../">
			<input type="hidden" name="op" value="guardarCambios">
			<input type="hidden" class="form-control" id="txtCodigo" name="txtCodigo"
		              value = "<?php echo $datos[0]['par_codigo']; ?>">
		<div class="form-group">
		    <label for="txtNumeroMax" class="col-lg-5 control-label">Num máx. libros a prestar</label>
		    <div class="col-lg-7">
		      <input type="number" min="1" max="99" required="required" class="form-control" id="txtNumeroMax" name="txtNumeroMax"
		            value = "<?php echo $datos[0]['par_num_maximo']; ?>">
		    </div>
		</div>

		  <div class="form-group">
		    <div class="col-lg-offset-2 col-lg-10 text-right">
		      	<button class="btnOperacion btn btn-primary"
					data-operacion="guardarCambios"
					data-destino="controlador/parametrosControlador.php"
					data-destino-form="vista/parametros/index.php">
					<!-- data-destino-form="vista/categorias/index.php"> -->
					Guardar cambios
				</button>
		    </div>
		  </div>
		</form>
	</div>
</div>