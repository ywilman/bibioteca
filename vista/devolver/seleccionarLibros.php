<?php
	$_POST['puntos'] = '../../';
	// require_once($_POST['puntos']. "controlador/librosControlador.php");
	require_once($_POST['puntos']. "controlador/usuarioControlador.php");
	require_once($_POST['puntos']. "controlador/prestamosControlador.php");

	// se registra los datos grales del prestamo
	$codigo = $_POST['codigo'];
	(new PrestamosControlador)->InsertarCodLector($codigo);

	$datosLector = (new UsuariosControladdor)->CargarPorId($codigo);
	$codPrestamo = (new PrestamosControlador)->CargarPorId($codigo);
	$datos = (new PrestamosControlador)->CargarLibrosDisponibles();
?>
<script>
$(document).ready(function() {
	$('#tabla').DataTable();
} );
</script>
<style type="text/css">
	img{
		cursor: pointer;
	}
</style>
<div class="alert alert-success">
  <strong> Prestar libros a: </strong> <?php echo $datosLector[0]['usl_nombre']. ' '. $datosLector[0]['usl_apellido']. ' Tel.: '. $datosLector[0]['usl_telefono']; ?>
</div>
<div class="panel panel-success">
	<div class="panel-heading"> Paso 2) Seleccione los libros </div>
	<div class="panel-body">
		<div class="table-responsive">
			<input type="hidden" name="txtPreCodigo" id="txtPreCodigo" value="<?php echo $codPrestamo[0]['pre_codigo']; ?>">
			<table class="table table-hover table-condensed " id="tabla">
				<thead>
					<tr>
						<th> </th>
						<th> Libro </th>
						<th> Descripcion </th>
						<th> Autor </th>
						<th> Disponibles </th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($datos as $value) { ?>
						<tr id="fila<?php echo $value['lib_codigo']; ?>">
							<td>
								<img src="vista/img/select_lector32.png"
									alt="Seleccionar libro"
									title="Seleccionar libro"
									class="btnPrestamo"
									data-operacion="pretarLibro"
									data-destino="controlador/prestamosControlador.php"
									data-destino-form="vista/prestamos/canastaLibros.php"
									id="<?php echo $value['lib_codigo']; ?>">
							</td>
							<td> <?php echo $value['lib_nombre']; ?> </td>
							<td> <?php echo $value['lib_descripcion']; ?> </td>
							<td> <?php echo $value['aut_nombre']; ?> </td>
							<td class="text-center" id="columna<?php echo $value['lib_codigo']; ?>"> <?php echo $value['lib_cant_disponible']; ?> </td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>