
    $(document).off("click",'.btnDevolver');
    $(document).on("click",'.btnDevolver', function(e){
        e.preventDefault();

        var operacion = $(this).attr('data-operacion');	// registrar, eliminar,  modificar o guardar cambios
        var destino = $(this).attr('data-destino');
        var formDestino = $(this).attr('data-destino-form');
        // alert('d '+return);
        var metodo = 'POST';
        var parametros = '';
        var puntos  = '../';

        var textoAlerta;
        var preCodigo = 0;
        if(operacion === "devolverLibros"){
            preCodigo = $(this).attr('id');
            parametros = {'preCodigo': preCodigo, 'op': 'devolverLibros', 'puntos': puntos};
            textoAlerta = "Los datos quedarán almacenados en el sistema";
            textoResultado = 'Registro almacenado con éxito.';

        }else if(operacion === "seleccionarLector"){
            var preCodigo = $(this).attr('id');

            $.post(formDestino, { 'puntos' : puntos, 'codigo': preCodigo }, function(data){
                $('#cargarTabla').html(data);
            });
            return;
        }else{
            textoAlerta="";
        }

        if (operacion != 'devolverLibros' ) {
            if (!confirm("¿ Está seguro de realizar esta operación ? " + textoAlerta)) {
            	return;
            }
        }

        $.ajax({
        	url: destino,
        	type: metodo,
        	data: parametros,
        	// dataType: 'json',
        	cache: false,
        })
        .done(function(result) {
        	if (result == 1) {
                if (operacion == 'devolverLibros') {
                    $.post(formDestino, {'codigo' : preCodigo}, function(respuesta) {
                        $('#cargarTabla').html('');
                    });
                    $("#fila" + preCodigo).remove();
                }

                $('#msjRespuesta').html(textoResultado);
                $("#divRespuesta").fadeTo(2000, 500).slideUp(500, function(){
                    $("#divRespuesta").slideUp(500);
                });
        	} else {
        		alert('Error inesperado, recargue la página y vuelva intentarlo.');
        	}
        })
        .fail(function() {
        	alert('Error inesperado al intentar comunicarse con el servidor.');
        })
    });