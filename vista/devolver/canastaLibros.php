<?php
	$_POST['puntos'] = '../../';
	require_once($_POST['puntos']. "controlador/devolverControlador.php");

	$codigo = $_POST['codigo'];
	$objPrestamo = new DevolverControlador();
	$detPrestamo = $objPrestamo->CargarCanasta($codigo);
	$datosLector = $objPrestamo->CargarLectorPendienteDeDevolverLibro($codigo);
?>
<script>
$(document).ready(function() {
	$('#tabla').DataTable();
} );
</script>

<div class="alert alert-info">
  <strong> Libros que se le han prestado a: </strong> <?php echo $datosLector[0]['usl_nombre']. ' '. $datosLector[0]['usl_apellido']. ' Tel.: '. $datosLector[0]['usl_telefono']; ?>
</div>
<div class="panel panel-info">
	<div class="panel-heading"> Libros prestados
		<button class="btn btn-success btnDevolver pull-right"
			data-operacion="devolverLibros"
			data-destino="controlador/devolverControlador.php"
			id="<?php echo $codigo; ?>">
			Devolver libros
		</button>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-hover table-condensed " id="tabla">
				<thead>
					<tr>
						<th> </th>
						<th> Libro </th>
						<th> Descripcion </th>
						<th> Autor </th>
						<!-- <th> Ejemplares </th> -->
					</tr>
				</thead>
				<tbody>
					<?php foreach ($detPrestamo as $value) { ?>
						<tr id="fila<?php echo $value['lib_codigo']; ?>">
							<td>
								<img src="vista/img/eliminar24.png"
									alt="Eliminar"
									title="Eliminar registro"
									class="btnOperacion"
									data-operacion="eliminar"
									data-destino="controlador/librosControlador.php"
									id="<?php echo $value['lib_codigo']; ?>">
								<img src="vista/img/edit24.png"
									alt="Modificar"
									title="Modificar registro"
									class="btnOperacion"
									data-operacion="modificar"
									data-destino="vista/libros/modificarLibro.php"
									id="<?php echo $value['lib_codigo']; ?>">
							</td>
							<td> <?php echo $value['lib_nombre']; ?> </td>
							<td> <?php echo $value['lib_descripcion']; ?> </td>
							<td> <?php echo $value['aut_nombre']; ?> </td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>