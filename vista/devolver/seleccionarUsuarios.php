<?php
	$_POST['puntos'] = '../../';
	require($_POST['puntos']. "controlador/devolverControlador.php");
	$datos = (new DevolverControlador)->CargarLectores();
?>
<script>
$(document).ready(function() {
	$('#tabla').DataTable();
} );
</script>
<style type="text/css">
	img{
		cursor: pointer;
	}
</style>
<div class="panel panel-info">
	<div class="panel-heading"> Paso 1) Seleccione el Lector </div>
	<div class="panel-body">
	<div class="table-responsive">
		<table class="table table-hover table-condensed " id="tabla">
			<thead>
				<tr>
					<th> </th>
					<th> Nombre </th>
					<th> Apellido </th>
					<th> Estado </th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($datos as $value) { ?>
					<tr id="fila<?php echo $value['pre_codigo']; ?>" class="<?php echo $value['detp_estado'] == 'Devolver' ? 'danger' : 'success'; ?>">
						<td class="text-center">
							<img src="vista/img/select_lector32.png"
								alt="Seleccionar"
								title="Seleccionar registro"
								class="btnDevolver"
								data-operacion="seleccionarLector"
								data-destino-form="vista/devolver/canastaLibros.php"
								id="<?php echo $value['pre_codigo']; ?>">
						</td>
						<td> <?php echo $value['usl_nombre']; ?> </td>
						<td> <?php echo $value['usl_apellido']; ?> </td>
						<td> <?php echo $value['detp_estado']; ?> </td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	</div>
</div>