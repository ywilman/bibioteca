
    $(document).off("click",'.btnPrestamo');
    $(document).on("click",'.btnPrestamo', function(e){
        e.preventDefault();

        var operacion = $(this).attr('data-operacion');	// registrar, eliminar,  modificar o guardar cambios
        var destino = $(this).attr('data-destino');
        var formDestino = $(this).attr('data-destino-form');
        // alert('d '+return);
        var metodo = 'POST';
        var parametros = '';
        var puntos  = '../';

        var idLibro = 0;
        if(operacion === "pretarLibro"){
            idLibro = $(this).attr('id');
        	var preCodigo = $('#txtPreCodigo').val();

            // validar que no se presten mas de lo permitido
            var totalLibros = eval($("#tablaLibrosPrestar tr").length);
            var maxLibPrestar = eval($('#spanMaxLibros').html());
            var cantidadDisponible = eval($('#columna' + idLibro).html());
            // alert('d'+totalLibros);
            if ((totalLibros - 1 ) >= maxLibPrestar) {
                $('#divRespuesta').removeClass('alert-success');
                $('#divRespuesta').addClass('alert-warning');
                $('#msjRespuesta').html('Ya tiene el núnermo máx. de libros a prestar.');
                $("#divRespuesta").fadeTo(2000, 500).slideUp(500, function(){
                    $("#divRespuesta").slideUp(500);
                });
                return;
            }
            if (cantidadDisponible <= 0 ) {
                $('#divRespuesta').removeClass('alert-success');
                $('#divRespuesta').addClass('alert-warning');
                $('#msjRespuesta').html('Ejemplares agotados de este libro.');
                $("#divRespuesta").fadeTo(2000, 500).slideUp(500, function(){
                    $("#divRespuesta").slideUp(500);
                });
                return;
            }

            parametros = {'idLibro': idLibro, 'preCodigo': preCodigo, 'op': 'pretarLibro', 'puntos': puntos};
            textoResultado = 'Libro agregado a canasta.';

        }else if(operacion === "eliminarLibCanasta"){
            idLibro = $(this).attr('id');
            parametros = {'codigo': idLibro, 'op': 'eliminarLibCanasta', 'puntos': puntos};
            textoResultado = 'Libro eliminado de canasta con éxito.';
        }else if(operacion === "seleccionarLector"){
            var idRegistro = $(this).attr('id');

            var clase = $('#fila'+idRegistro).attr('class');
            if (clase.indexOf("danger") > -1) {
                $('#divRespuesta').removeClass('alert-success');
                $('#divRespuesta').addClass('alert-warning');
                $('#msjRespuesta').html('Este lector está pendiente de devolver libros.');
                $("#divRespuesta").fadeTo(2000, 500).slideUp(500, function(){
                    $("#divRespuesta").slideUp(500);
                });
                return;
            }

            $.post(destino, { 'puntos' : puntos, 'codigo': idRegistro }, function(data){
                $('#cargarForm').html(data);
            });

            $.post(formDestino, { 'puntos' : puntos, 'codigo': idRegistro }, function(data){
                $('#cargarTabla').html(data);
            });
            return;
        }

        $.ajax({
        	url: destino,
        	type: metodo,
        	data: parametros,
        	// dataType: 'json',
        	cache: false,
        })
        .done(function(result) {
        	if (result == 1) {
                if (operacion == 'pretarLibro') {
                    $.post(formDestino, {'codigo' : preCodigo}, function(respuesta) {
                        $('#cargarTabla').html(respuesta);
                    });
                    var cantidadActual = $('#columna' + idLibro).html();
                    var nuevaCantidad = cantidadActual - 1;
                    $('#columna' + idLibro).html(nuevaCantidad);
                    if (nuevaCantidad == 0) {
                        $('#fila' + idLibro).removeClass('warning');
                        $('#fila' + idLibro).addClass('danger');
                    }
                    // $('#txtNombre').val('');
                    // $('#txtDescripcion').val('');
                }

                if (operacion  === 'eliminarLibCanasta') {
                    $("#filaCanasta" + idLibro).remove();
                    var cantidadActual = eval($('#columna' + idLibro).html());
                    var nuevaCantidad = cantidadActual + 1;
                    $('#columna' + idLibro).html(nuevaCantidad);
                    // if (nuevaCantidad == 0) {
                    //     $('#fila' + idLibro).removeClass('warning');
                    //     $('#fila' + idLibro).addClass('danger');
                    // }
                    // $('#msjRespuesta').html(textoResultado);
                }

                if (operacion  === 'guardarCambios') {
                    $.post(formDestino, {}, function(respuesta) {
                        $('#contenido').html(respuesta);
                    });
                    // $('#msjRespuesta').html(textoResultado);
                }

                $('#divRespuesta').removeClass('alert-warning');
                $('#divRespuesta').addClass('alert-success');

                $('#msjRespuesta').html(textoResultado);
                $("#divRespuesta").fadeTo(2000, 500).slideUp(500, function(){
                    $("#divRespuesta").slideUp(500);
                });
        	} else {
        		alert('Error inesperado, recargue la página y vuelva intentarlo.');
        	}
        })
        .fail(function() {
        	alert('Error inesperado al intentar comunicarse con el servidor.');
        })
    });