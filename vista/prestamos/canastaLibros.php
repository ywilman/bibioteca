<?php
	$_POST['puntos'] = '../../';
	require_once($_POST['puntos']. "controlador/prestamosControlador.php");

	// se registra los datos grales del prestamo
	$codigo      = $_POST['codigo'];
	$objPrestamo = new PrestamosControlador();
	$detPrestamo = $objPrestamo->CargarCanasta($codigo);
	$maxLibros   = $objPrestamo->MaxLibrosAPrestar();
?>
<script>
$(document).ready(function() {
	$('#tablaLibrosPresta').DataTable();
} );
</script>

<div class="alert alert-warning">
  <strong> Número Máximo de Libros a prestar: </strong> <span id="spanMaxLibros"> <?php echo $maxLibros[0]['par_num_maximo']; ?> </span>
</div>
<div class="panel panel-primary">
	<div class="panel-heading"> Libros a prestar </div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-hover table-condensed " id="tablaLibrosPrestar">
				<thead>
					<tr>
						<th> </th>
						<th> Libro </th>
						<th> Descripcion </th>
						<th> Autor </th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($detPrestamo as $value) { ?>
						<tr id="filaCanasta<?php echo $value['lib_codigo']; ?>">
							<td>
								<img src="vista/img/eliminar24.png"
									alt="Eliminar"
									title="Eliminar registro"
									class="btnPrestamo"
									data-operacion="eliminarLibCanasta"
									data-destino="controlador/prestamosControlador.php"
									id="<?php echo $value['lib_codigo']; ?>">
							</td>
							<td> <?php echo $value['lib_nombre']; ?> </td>
							<td> <?php echo $value['lib_descripcion']; ?> </td>
							<td> <?php echo $value['aut_nombre']; ?> </td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>