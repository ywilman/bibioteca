<?php
	$_POST['puntos'] = '../../';
	require($_POST['puntos']. "controlador/prestamosControlador.php");
	$datos = (new PrestamosControlador)->CargarLectores();
?>
<script>
$(document).ready(function() {
	$('#tabla').DataTable();
} );
</script>
<style type="text/css">
	img{
		cursor: pointer;
	}
</style>
<div class="panel panel-primary">
	<div class="panel-heading"> Paso 1) Seleccione el Lector </div>
	<div class="panel-body">
	<div class="table-responsive">
		<table class="table table-hover table-condensed " id="tabla">
			<thead>
				<tr>
					<th> </th>
					<th> Nombre </th>
					<th> Apellido </th>
					<th> Estado </th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($datos as $value) { ?>
					<tr id="fila<?php echo $value['usl_codigo']; ?>" class="<?php echo $value['detp_estado'] == 'Devolver' ? 'danger' : 'success'; ?>">
						<td class="text-center">
							<img src="vista/img/select_lector32.png"
								alt="Seleccionar"
								title="Seleccionar registro"
								class="btnPrestamo"
								data-operacion="seleccionarLector"
								data-destino="vista/prestamos/seleccionarlibros.php"
								data-destino-form="vista/prestamos/canastaLibros.php"
								id="<?php echo $value['usl_codigo']; ?>">
						</td>
						<td> <?php echo $value['usl_nombre']; ?> </td>
						<td> <?php echo $value['usl_apellido']; ?> </td>
						<td> <?php echo $value['detp_estado']; ?> </td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	</div>
</div>