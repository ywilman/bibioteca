<!DOCTYPE html>
<html>
<head>
	<title>  </title>
</head>
<body>
	<div class="row">
		<div class="col-sm-6">
			<h1> Lista de categorías de libros </h1>
		</div>
		<!-- <div class="col-sm-6">
			<div class="alert alert-dismissible alert-success" id="divRespuesta">
		    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		    	<label id="msjRespuesta"> This alert box could indicate a successful or positive action.</label>
		 	 </div>
		</div> -->
	</div>

	<div class="row">
		<div class="col-md-6">
			<div id="cargarForm">
				<?php include('registrarCategoria.php'); ?>
			</div>
		</div>

		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading"> Lista de categorías </div>
				<div class="panel-body" id="cargarTabla">
					<?php include('mostrarCategorias.php'); ?>
				</div>
			</div>
		</div>
	</div>
</body>
</html>