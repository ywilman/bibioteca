<?php
	$_POST['puntos'] = '../../';
	require($_POST['puntos']. "controlador/categoriasControlador.php");
	$datos = Cargar();
?>
<div class="table-responsive">
	<table class="table table-hover table-condensed ">
		<thead>
			<tr>
				<th> </th>
				<th> Nombre </th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($datos as $value) { ?>
				<tr id="fila<?php echo $value['cat_codigo']; ?>">
					<td>
						<img src="vista/img/eliminar24.png"
							alt="Eliminar"
							title="Eliminar registro"
							class="btnOperacion"
							data-operacion="eliminar"
							data-destino="controlador/categoriasControlador.php"
							id="<?php echo $value['cat_codigo']; ?>">
						<img src="vista/img/edit24.png"
							alt="Modificar"
							title="Modificar registro"
							class="btnOperacion"
							data-operacion="modificar"
							data-destino="vista/categorias/modificarCategoria.php"
							id="<?php echo $value['cat_codigo']; ?>">
					</td>
					<td> <?php echo $value['cat_nombre']; ?> </td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>