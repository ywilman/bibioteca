
<?php
$codigo = $_POST['codigo'];
$_POST['puntos'] = '../../';
require($_POST['puntos']. "controlador/categoriasControlador.php");
$datos = CargarPorId($codigo);

 ?>
<div class="panel panel-warning">
	<div class="panel-heading"> Modifcar de categoría </div>
	<div class="panel-body">

		<form action="#" method="post" name="form" class="FormularioAjax form-horizontal" enctype="multipart/form-data" role="form">
			<input type="hidden" name="puntos" value="../">
			<input type="hidden" name="op" value="guardarCambios">
		<div class="form-group">
		    <label for="txtNombre" class="col-lg-2 control-label"> Categoría </label>
		    <div class="col-lg-10">
		      	<input type="text" class="form-control" id="txtNombre" name="txtNombre"
		             placeholder="Nombre" value="<?php echo $datos[0]['cat_nombre']; ?>">
		        <input type="hidden" class="form-control" id="txtCodigo" name="txtCodigo"
		              value="<?php echo $codigo; ?>">
		    </div>
		</div>

		  <div class="form-group">
		    <div class="col-lg-offset-2 col-lg-10">
		      	<button class="btnOperacion btn btn-warning"
					data-operacion="guardarCambios"
					data-destino="controlador/categoriasControlador.php"
					data-destino-form="vista/categorias/index.php">
					Guardar cambios
				</button>
				<button class="btnOperacion btn btn-default"
					data-operacion="cancelar"
					data-destino="vista/categorias/registrarCategoria.php">
					Cancelar
				</button>
		    </div>
		  </div>
		</form>
	</div>
</div>