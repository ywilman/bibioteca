<?php
	$_POST['puntos'] = '../../';
	require($_POST['puntos']. "controlador/usuarioControlador.php");
	$datos = (new UsuariosControladdor)->Cargar();
?>
<div class="table-responsive">
	<table class="table table-hover table-condensed ">
		<thead>
			<tr>
				<th> </th>
				<th> Nombre </th>
				<th> Apellido </th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($datos as $value) { ?>
				<tr id="fila<?php echo $value['usl_codigo']; ?>">
					<td>
						<img src="vista/img/eliminar24.png"
							alt="Eliminar"
							title="Eliminar registro"
							class="btnOperacion"
							data-operacion="eliminar"
							data-destino="controlador/autorControlador.php"
							id="<?php echo $value['usl_codigo']; ?>">
						<img src="vista/img/edit24.png"
							alt="Modificar"
							title="Modificar registro"
							class="btnOperacion"
							data-operacion="modificar"
							data-destino="vista/usuarios_libros/modificarUsuario.php"
							id="<?php echo $value['usl_codigo']; ?>">
					</td>
					<td> <?php echo $value['usl_nombre']; ?> </td>
					<td> <?php echo $value['usl_apellido']; ?> </td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>