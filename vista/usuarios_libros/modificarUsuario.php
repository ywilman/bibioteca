
<?php
$codigo = $_POST['codigo'];
$_POST['puntos'] = '../../';
require($_POST['puntos']. "controlador/usuarioControlador.php");
$datos = (new UsuariosControladdor)->CargarPorId($codigo);

 ?>
<div class="panel panel-warning">
	<div class="panel-heading"> Modifcar de usuario </div>
	<div class="panel-body">

		<form action="#" method="post" name="form" class="FormularioAjax form-horizontal" enctype="multipart/form-data" role="form">
			<input type="hidden" name="puntos" value="../">
			<input type="hidden" name="op" value="guardarCambios">
		<div class="form-group">
		    <label for="txtNombre" class="col-lg-2 control-label">Nombre </label>
		    <div class="col-lg-10">
		      	<input type="text" class="form-control" id="txtNombre" name="txtNombre"
		             placeholder="Nombre" value="<?php echo $datos[0]['usl_nombre']; ?>">
		        <input type="hidden" class="form-control" id="txtCodigo" name="txtCodigo"
		              value="<?php echo $codigo; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="txtApellido" class="col-lg-2 control-label">Apellido </label>
		    <div class="col-lg-10">
		      <input type="text" class="form-control" id="txtApellido" name="txtApellido"
		             placeholder="Apellido" value="<?php echo $datos[0]['usl_apellido']; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="txtTelefono" class="col-lg-2 control-label"> Teléfono </label>
		    <div class="col-lg-10">
		      <input type="text" class="form-control" id="txtTelefono" name="txtTelefono"
		             placeholder="Teléfono" value="<?php echo $datos[0]['usl_telefono']; ?>">
		    </div>
		</div>

		  <div class="form-group">
		    <div class="col-lg-offset-2 col-lg-10">
		      	<button class="btnOperacion btn btn-warning"
					data-operacion="guardarCambios"
					data-destino="controlador/usuarioControlador.php"
					data-destino-form="vista/usuarios_libros/index.php">
					Guardar cambios
				</button>
				<button class="btnOperacion btn btn-default"
					data-operacion="cancelar"
					data-destino="vista/usuarios_libros/registrarUsuario.php">
					Cancelar
				</button>
		    </div>
		  </div>
		</form>
	</div>
</div>