<?php
	$_POST['puntos'] = '../../';
	require_once($_POST['puntos']. "controlador/autorControlador.php");
	$datos = (new Autores())->Cargar();
?>
<div class="table-responsive">
	<table class="table table-hover table-condensed ">
		<thead>
			<tr>
				<th> </th>
				<th> Nombre </th>
				<th> Apellido </th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($datos as $value) { ?>
				<tr id="fila<?php echo $value['aut_codigo']; ?>">
					<td>
						<img src="vista/img/eliminar24.png"
							alt="Eliminar"
							title="Eliminar registro"
							class="btnOperacion"
							data-operacion="eliminar"
							data-destino="controlador/autorControlador.php"
							id="<?php echo $value['aut_codigo']; ?>">
						<img src="vista/img/edit24.png"
							alt="Modificar"
							title="Modificar registro"
							class="btnOperacion"
							data-operacion="modificar"
							data-destino="vista/autores/modificarAutor.php"
							id="<?php echo $value['aut_codigo']; ?>">
					</td>
					<td> <?php echo $value['aut_nombre']; ?> </td>
					<td> <?php echo $value['aut_apellido']; ?> </td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>