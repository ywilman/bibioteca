<?php
	$_POST['puntos'] = '../../';
	require_once($_POST['puntos']. "controlador/autorControlador.php");

	$datosAutores = (new Autores)->Cargar();
?>
<div class="panel panel-primary">
	<div class="panel-heading"> Registro de Libros </div>
	<div class="panel-body">

		<form action="#" method="post" name="form" class="FormularioAjax form-horizontal" enctype="multipart/form-data" role="form">
			<input type="hidden" name="puntos" value="../">
			<input type="hidden" name="op" value="registrar">
		<div class="form-group">
		    <label for="txtNombre" class="col-lg-2 control-label"> Nombre</label>
		    <div class="col-lg-10">
		      <input type="text" class="form-control" id="txtNombre" name="txtNombre"
		             placeholder="Nombre">
		    </div>
		</div>
	  	<div class="form-group">
		    <label for="txtDescripcion" class="col-lg-2 control-label"> Descripción </label>
		    <div class="col-lg-10">
		      <input type="text" class="form-control" id="txtDescripcion" name="txtDescripcion"
		             placeholder="Descripción">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="txtCantEjemplares" class="col-lg-2 control-label"> Ejemplares </label>
		    <div class="col-lg-10">
		      <input type="text" class="form-control" id="txtCantEjemplares" name="txtCantEjemplares"
		             placeholder="Ejemplares">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="txtCantEjemplares" class="col-lg-2 control-label"> Autor </label>
		    <div class="col-lg-10">
		    	<select class="form-control" name="selAutor" id="selAutor">
		    		<?php foreach ($datosAutores as $value): ?>
		    			<option value="0"> Seleccione </option>
		    			<option value="<?php echo $value['aut_codigo']; ?>"> <?php echo $value['aut_nombre']; ?> </option>
		    		<?php endforeach ?>
		    	</select>
		    </div>
	  	</div>

		  <div class="form-group">
		    <div class="col-lg-offset-2 col-lg-10">
		      	<button class="btnOperacion btn btn-primary"
					data-operacion="registrar"
					data-destino="controlador/librosControlador.php"
					data-destino-form="vista/libros/mostrarLibros.php">
					Guardar
				</button>
		    </div>
		  </div>
		</form>
	</div>
</div>