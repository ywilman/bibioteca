<!DOCTYPE html>
<html>
<head>
	<title>  </title>
	<style type="text/css">
		#divRespuesta{
			display: none;
		}
	</style>
</head>
<body>
	<div class="row">
		<div class="col-sm-6">
			<h1> Lista de test registrado </h1>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<div id="cargarForm">
				<?php include('registrarLibro.php'); ?>
			</div>
		</div>

		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading"> Lista de test </div>
				<div class="panel-body" id="cargarTabla">
					<?php include('mostrarLibros.php'); ?>
				</div>
			</div>
		</div>
	</div>
</body>
</html>