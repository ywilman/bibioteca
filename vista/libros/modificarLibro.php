
<?php
$codigo = $_POST['codigo'];
$_POST['puntos'] = '../../';
	require_once($_POST['puntos']. "controlador/librosControlador.php");
	require_once($_POST['puntos']. "controlador/autorControlador.php");
	$datosAutores = (new Autores())->Cargar();

$datos = (new LibrosControladdor)->CargarPorId($codigo);
// print_r($datos);
 ?>
<div class="panel panel-warning">
	<div class="panel-heading"> Modifcar de Libro </div>
	<div class="panel-body">

		<form action="#" method="post" name="form" class="FormularioAjax form-horizontal" enctype="multipart/form-data" role="form">
			<input type="hidden" name="puntos" value="../">
			<input type="hidden" name="op" value="guardarCambios">
		<div class="form-group">
		    <label for="txtNombre" class="col-lg-2 control-label">Nombre </label>
		    <div class="col-lg-10">
		      	<input type="text" class="form-control" id="txtNombre" name="txtNombre"
		             placeholder="Nombre" value="<?php echo $datos[0]['lib_nombre']; ?>">
		        <input type="hidden" class="form-control" id="txtCodigo" name="txtCodigo"
		              value="<?php echo $codigo; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="txtDescripcion" class="col-lg-2 control-label">Descripcion </label>
		    <div class="col-lg-10">
		      <input type="text" class="form-control" id="txtDescripcion" name="txtDescripcion"
		             placeholder="Descripción" value="<?php echo $datos[0]['lib_descripcion']; ?>">
		    </div>
		</div>

	  	<div class="form-group">
		    <label for="txtCantEjemplares" class="col-lg-2 control-label"> Ejemplares </label>
		    <div class="col-lg-10">
		      <input type="text" class="form-control" id="txtCantEjemplares" name="txtCantEjemplares"
		             placeholder="Ejemplares" value="<?php echo $datos[0]['lib_cant_ejemplares']; ?>">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="txtCantEjemplares" class="col-lg-2 control-label"> Autor </label>
		    <div class="col-lg-10">
		    	<select class="form-control" name="selAutor" id="selAutor">
		    		<option value="0"> Seleccione </option>
		    		<?php foreach ($datosAutores as $value): ?>
		    			<option value="<?php echo $value['aut_codigo']; ?>" <?php echo $value['aut_codigo'] == $datos[0]['lib_codaut'] ? 'selected'  : '' ;?>> <?php echo $value['aut_nombre']; ?> </option>
		    		<?php endforeach ?>
		    	</select>
		    </div>
	  	</div>


		  <div class="form-group">
		    <div class="col-lg-offset-2 col-lg-10">
		      	<button class="btnOperacion btn btn-warning"
					data-operacion="guardarCambios"
					data-destino="controlador/librosControlador.php"
					data-destino-form="vista/libros/index.php">
					Guardar cambios
				</button>
				<button class="btnOperacion btn btn-default"
					data-operacion="cancelar"
					data-destino="vista/libros/registrarLibro.php">
					Cancelar
				</button>
		    </div>
		  </div>
		</form>
	</div>
</div>