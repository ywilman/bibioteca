<?php
	$_POST['puntos'] = '../../';
	require_once($_POST['puntos']. "controlador/librosControlador.php");

	$datos = (new LibrosControladdor)->Cargar();
?>
<script>
$(document).ready(function() {
	$('#tabla').DataTable();
} );
</script>
	<!-- <script>
	  $(function () {
	    $('#tabla').DataTable({
	      'paging'      : true,
	      'lengthChange': false,
	      'searching'   : false,
	      'ordering'    : true,
	      'info'        : true,
	      'autoWidth'   : false
	    })
	  })
	</script> -->
<div class="table-responsive">
	<table class="table table-hover table-condensed " id="tabla">
		<thead>
			<tr>
				<th> </th>
				<th> Libro </th>
				<th> Descripcion </th>
				<th> Autor </th>
				<th> Ejemplares </th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($datos as $value) { ?>
				<tr id="fila<?php echo $value['lib_codigo']; ?>">
					<td>
						<img src="vista/img/eliminar24.png"
							alt="Eliminar"
							title="Eliminar registro"
							class="btnOperacion"
							data-operacion="eliminar"
							data-destino="controlador/librosControlador.php"
							id="<?php echo $value['lib_codigo']; ?>">
						<img src="vista/img/edit24.png"
							alt="Modificar"
							title="Modificar registro"
							class="btnOperacion"
							data-operacion="modificar"
							data-destino="vista/libros/modificarLibro.php"
							id="<?php echo $value['lib_codigo']; ?>">
					</td>
					<td> <?php echo $value['lib_nombre']; ?> </td>
					<td> <?php echo $value['lib_descripcion']; ?> </td>
					<td> <?php echo $value['aut_nombre']; ?> </td>
					<td> <?php echo $value['lib_cant_ejemplares']; ?> </td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>