<?php
$puntos = isset($_POST['puntos']) ? $_POST['puntos'] : '../';
require_once($puntos. 'modelo/conxion.php');

$operacion = isset($_POST['op']) ? $_POST['op'] : '';

if ($operacion == 'registrar') {
	$obj = new Conexion();

	$parametros = [
		":lib_nombre" => $_POST['txtNombre'],
		":lib_descripcion" => $_POST['txtDescripcion'],
		":lib_cant_ejemplares" => $_POST['txtCantEjemplares'],
		":lib_codaut" => $_POST['selAutor']
	];

	$query = 'INSERT INTO lib_libros (lib_nombre, lib_descripcion, lib_cant_ejemplares, lib_codaut)
				VALUES (:lib_nombre, :lib_descripcion, :lib_cant_ejemplares, :lib_codaut)';

	echo $obj->Ejecutar($query, $parametros) ? '1' : 'error';
}

if ($operacion == 'eliminar') {
	$obj = new Conexion();

	$parametros = [
		":lib_codigo" => $_POST['codigo']
	];

	$query = 'DELETE FROM lib_libros WHERE lib_codigo = :lib_codigo';
	echo $obj->Ejecutar($query, $parametros) ? '1' : 'error';
}

if ($operacion == 'guardarCambios') {
	$obj = new Conexion();

	$parametros = [
		":lib_codigo" => $_POST['txtCodigo'],
		":lib_nombre" => $_POST['txtNombre'],
		":lib_descripcion" => $_POST['txtDescripcion'],
		":lib_cant_ejemplares" => $_POST['txtCantEjemplares'],
		":lib_codaut" => $_POST['selAutor']
	];

	$query = 'UPDATE lib_libros SET lib_nombre = :lib_nombre, lib_descripcion = :lib_descripcion,
				lib_cant_ejemplares = :lib_cant_ejemplares, lib_codaut = :lib_codaut
				WHERE lib_codigo = :lib_codigo';

	echo $obj->Ejecutar($query, $parametros) ? '1' : 'error';
}

/**
 *
 */
class LibrosControladdor
{
	function Cargar(){
		$sql = 'SELECT lib_codigo, lib_nombre, lib_descripcion, lib_cant_ejemplares, aut_nombre
					FROM lib_libros
					LEFT JOIN aut_autores ON lib_codaut = aut_codigo
					ORDER BY lib_nombre';
		$obj = new Conexion();
		$datos = $obj->CargarDatos($sql);

		return $datos;
	}

	function CargarPorId($codigo){
		$parametros = [
			":lib_codigo" => $codigo
		];
		$sql = 'SELECT lib_codigo, lib_nombre, lib_descripcion, lib_cant_ejemplares, lib_codaut
					FROM lib_libros WHERE lib_codigo = :lib_codigo LIMIT 1';
		$obj = new Conexion();
		$datos = $obj->CargarDatos($sql, $parametros);

		return $datos;
	}
}
 ?>