<?php
$puntos = isset($_POST['puntos']) ? $_POST['puntos'] : '';
require($puntos. 'modelo/conxion.php');

$operacion = isset($_POST['op']) ? $_POST['op'] : '';

if ($operacion == 'registrar') {
	$obj = new Conexion();

	$parametros = [
		":cat_nombre" => $_POST['txtNombre']
	];

	$query = 'INSERT INTO cat_categorias (cat_nombre) VALUES (:cat_nombre)';

	echo $obj->Ejecutar($query, $parametros) ? '1' : 'error';
}

if ($operacion == 'eliminar') {
	$obj = new Conexion();

	$parametros = [
		":cat_codigo" => $_POST['codigo']
	];

	$query = 'DELETE FROM cat_categorias WHERE cat_codigo = :cat_codigo';
	echo $obj->Ejecutar($query, $parametros) ? '1' : 'error';
}

if ($operacion == 'guardarCambios') {
	$obj = new Conexion();

	$parametros = [
		":cat_codigo" => $_POST['txtCodigo'],
		":cat_nombre" => $_POST['txtNombre']
	];

	$query = 'UPDATE cat_categorias SET cat_nombre = :cat_nombre
				WHERE cat_codigo = :cat_codigo';

	echo $obj->Ejecutar($query, $parametros) ? '1' : 'error';
}

function Cargar(){
	$sql = 'SELECT cat_codigo, cat_nombre FROM cat_categorias';
	$obj = new Conexion();
	$datos = $obj->CargarDatos($sql);

	return $datos;
}

function CargarPorId($codigo){
	$parametros = [
		":cat_codigo" => $codigo
	];
	$sql = 'SELECT cat_codigo, cat_nombre FROM cat_categorias WHERE cat_codigo = :cat_codigo';
	$obj = new Conexion();
	$datos = $obj->CargarDatos($sql, $parametros);

	return $datos;
}
 ?>