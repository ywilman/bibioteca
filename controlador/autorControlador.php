<?php
$puntos = isset($_POST['puntos']) ? $_POST['puntos'] : '';
require_once($puntos. 'modelo/conxion.php');

$operacion = isset($_POST['op']) ? $_POST['op'] : '';

if ($operacion == 'registrar') {
	$obj = new Conexion();

	$parametros = [
		":aut_nombre" => $_POST['txtNombre'],
		":aut_apellido" => $_POST['txtApellido']
	];

	$query = 'INSERT INTO aut_autores (aut_nombre, aut_apellido) VALUES (:aut_nombre, :aut_apellido)';

	echo $obj->Ejecutar($query, $parametros) ? '1' : 'error';
}

if ($operacion == 'eliminar') {
	$obj = new Conexion();

	$parametros = [
		":aut_codigo" => $_POST['codigo']
	];

	$query = 'DELETE FROM aut_autores WHERE aut_codigo = :aut_codigo';
	echo $obj->Ejecutar($query, $parametros) ? '1' : 'error';
}

if ($operacion == 'guardarCambios') {
	$obj = new Conexion();

	$parametros = [
		":aut_codigo" => $_POST['txtCodigo'],
		":aut_nombre" => $_POST['txtNombre'],
		":aut_apellido" => $_POST['txtApellido']
	];

	$query = 'UPDATE aut_autores SET aut_nombre = :aut_nombre, aut_apellido = :aut_apellido
				WHERE aut_codigo = :aut_codigo';

	echo $obj->Ejecutar($query, $parametros) ? '1' : 'error';
}
class Autores
{
	function Cargar(){
		$sql = 'SELECT aut_codigo, aut_nombre, aut_apellido FROM aut_autores';
		$obj = new Conexion();
		$datos = $obj->CargarDatos($sql);

		return $datos;
	}

	function CargarPorId($codigo){
		$parametros = [
			":aut_codigo" => $codigo
		];
		$sql = 'SELECT aut_codigo, aut_nombre, aut_apellido FROM aut_autores WHERE aut_codigo = :aut_codigo';
		$obj = new Conexion();
		$datos = $obj->CargarDatos($sql, $parametros);

		return $datos;
	}
}
 ?>