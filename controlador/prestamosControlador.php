<?php
$puntos = isset($_POST['puntos']) ? $_POST['puntos'] : '';
require_once($puntos. 'modelo/conxion.php');

$operacion = isset($_POST['op']) ? $_POST['op'] : '';

if ($operacion == 'pretarLibro') {
	$obj = new Conexion();

	$cantidad = 1;
	$parametros = [
		":detp_codpre" => $_POST['preCodigo'],
		":detp_codlib" => $_POST['idLibro'],
		":detp_cantidad" => $cantidad,
		":detp_estado" => 'Devolver'
	];

	$query = 'INSERT INTO detp_detalle_prestamo (detp_codpre, detp_codlib, detp_cantidad, detp_estado)
					VALUES (:detp_codpre, :detp_codlib, :detp_cantidad, :detp_estado)';

	echo $obj->Ejecutar($query, $parametros) ? '1' : 'error';
}

if ($operacion == 'eliminarLibCanasta') {
	$obj = new Conexion();

	$parametros = [
		":detp_codlib" => $_POST['codigo']
	];

	$query = 'DELETE FROM detp_detalle_prestamo WHERE detp_codlib = :detp_codlib';
	echo $obj->Ejecutar($query, $parametros) ? '1' : 'error';
}

if ($operacion == 'guardarCambios') {
	$obj = new Conexion();

	$parametros = [
		":pre_codigo" => $_POST['txtCodigo'],
		":pre_codusl" => $_POST['txtNumeroMax']
	];

	$query = 'UPDATE pre_prestamos SET pre_codusl = :pre_codusl
				WHERE pre_codigo = :pre_codigo';

	echo $obj->Ejecutar($query, $parametros) ? '1' : 'error';
}

/**
 *
 */
class PrestamosControlador
{
	function InsertarCodLector($codusl){
		$obj = new Conexion();

		$parametros = [
			":pre_codusl" => $codusl
		];

		$query = 'INSERT INTO pre_prestamos (pre_codusl) VALUES (:pre_codusl)';

		$obj->Ejecutar($query, $parametros) ? '1' : 'error';
	}

	function CargarLectores(){
		$sql = 'SELECT DISTINCT usl_codigo, usl_nombre, usl_apellido, usl_telefono, detp_estado
			FROM usl_usuarios_libros
            LEFT JOIN pre_prestamos ON pre_codusl = usl_codigo
			LEFT JOIN detp_detalle_prestamo ON pre_codigo = detp_codpre
			LEFT JOIN lib_libros ON lib_codigo = detp_codlib
			GROUP BY usl_codigo
			';
		$obj = new Conexion();
		$datos = $obj->CargarDatos($sql);

		return $datos;
	}

	function CargarCanasta($codusl){
		$sql = 'SELECT lib_codigo, lib_nombre, lib_descripcion, aut_nombre
			FROM pre_prestamos
			INNER JOIN detp_detalle_prestamo ON pre_codigo = detp_codpre
			INNER JOIN lib_libros ON lib_codigo = detp_codlib
			LEFT JOIN aut_autores ON lib_codaut = aut_codigo
			WHERE pre_codigo = '.$codusl;
		$obj = new Conexion();
		$datos = $obj->CargarDatos($sql);

		return $datos;
	}

	function CargarLibrosDisponibles(){
		$sql = 'SELECT lib_codigo, lib_nombre, lib_descripcion, (lib_cant_ejemplares - (SUM(IFNULL(detp_cantidad, 0)))) AS lib_cant_disponible, aut_nombre
					FROM lib_libros
					LEFT JOIN detp_detalle_prestamo ON lib_codigo = detp_codlib
					LEFT JOIN aut_autores ON lib_codaut = aut_codigo
                    GROUP BY lib_codigo
					ORDER BY lib_nombre';
		$obj = new Conexion();
		$datos = $obj->CargarDatos($sql);

		return $datos;
	}

	function CargarPorId($codigo){
		$parametros = [
			":pre_codusl" => $codigo
		];
		$sql = 'SELECT MAX(pre_codigo) as pre_codigo FROM pre_prestamos WHERE pre_codusl = :pre_codusl  LIMIT 1';
		$obj = new Conexion();
		$datos = $obj->CargarDatos($sql, $parametros);

		return $datos;
	}

	function MaxLibrosAPrestar(){
		$sql = 'SELECT par_num_maximo FROM par_parametros LIMIT 1';
		$obj = new Conexion();
		$datos = $obj->CargarDatos($sql);

		return $datos;
	}
}
 ?>