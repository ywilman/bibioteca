<?php
$puntos = isset($_POST['puntos']) ? $_POST['puntos'] : '';
require($puntos. 'modelo/conxion.php');

$operacion = isset($_POST['op']) ? $_POST['op'] : '';

if ($operacion == 'registrar') {
	$obj = new Conexion();

	$parametros = [
		":par_num_maximo" => $_POST['txtNumeroMax']
	];

	$query = 'INSERT INTO par_parametros (par_num_maximo) VALUES (:par_num_maximo)';

	echo $obj->Ejecutar($query, $parametros) ? '1' : 'error';
}

if ($operacion == 'eliminar') {
	$obj = new Conexion();

	$parametros = [
		":par_codigo" => $_POST['codigo']
	];

	$query = 'DELETE FROM par_parametros WHERE par_codigo = :par_codigo';
	echo $obj->Ejecutar($query, $parametros) ? '1' : 'error';
}

if ($operacion == 'guardarCambios') {
	$obj = new Conexion();

	$parametros = [
		":par_codigo" => $_POST['txtCodigo'],
		":par_num_maximo" => $_POST['txtNumeroMax']
	];

	$query = 'UPDATE par_parametros SET par_num_maximo = :par_num_maximo
				WHERE par_codigo = :par_codigo';

	echo $obj->Ejecutar($query, $parametros) ? '1' : 'error';
}

function Cargar(){
	$sql = 'SELECT par_codigo, par_num_maximo FROM par_parametros LIMIT 1';
	$obj = new Conexion();
	$datos = $obj->CargarDatos($sql);

	return $datos;
}

function CargarPorId($codigo){
	$parametros = [
		":par_codigo" => $codigo
	];
	$sql = 'SELECT par_codigo, par_num_maximo FROM par_parametros WHERE par_codigo = :par_codigo';
	$obj = new Conexion();
	$datos = $obj->CargarDatos($sql, $parametros);

	return $datos;
}
 ?>