<?php
$puntos = isset($_POST['puntos']) ? $_POST['puntos'] : '';
require_once($puntos. 'modelo/conxion.php');

$operacion = isset($_POST['op']) ? $_POST['op'] : '';

if ($operacion == 'registrar') {
	$obj = new Conexion();

	$parametros = [
		":usl_nombre" => $_POST['txtNombre'],
		":usl_apellido" => $_POST['txtApellido'],
		":usl_telefono" => $_POST['txtTelefono']
	];

	$query = 'INSERT INTO usl_usuarios_libros (usl_nombre, usl_apellido, usl_telefono) VALUES (:usl_nombre, :usl_apellido, :usl_telefono)';

	echo $obj->Ejecutar($query, $parametros) ? '1' : 'error';
}

if ($operacion == 'eliminar') {
	$obj = new Conexion();

	$parametros = [
		":usl_codigo" => $_POST['codigo']
	];

	$query = 'DELETE FROM usl_usuarios_libros WHERE usl_codigo = :usl_codigo';
	echo $obj->Ejecutar($query, $parametros) ? '1' : 'error';
}

if ($operacion == 'guardarCambios') {
	$obj = new Conexion();

	$parametros = [
		":usl_codigo" => $_POST['txtCodigo'],
		":usl_nombre" => $_POST['txtNombre'],
		":usl_apellido" => $_POST['txtApellido'],
		":usl_telefono" => $_POST['txtTelefono']
	];

	$query = 'UPDATE usl_usuarios_libros SET usl_nombre = :usl_nombre, usl_apellido = :usl_apellido, usl_telefono = :usl_telefono
				WHERE usl_codigo = :usl_codigo';

	echo $obj->Ejecutar($query, $parametros) ? '1' : 'error';
}

class UsuariosControladdor {
	function Cargar(){
		$sql = 'SELECT usl_codigo, usl_nombre, usl_apellido, usl_telefono FROM usl_usuarios_libros ORDER BY usl_nombre';
		$obj = new Conexion();
		$datos = $obj->CargarDatos($sql);

		return $datos;
	}

	function CargarPorId($codigo){
		$parametros = [
			":usl_codigo" => $codigo
		];
		$sql = 'SELECT usl_codigo, usl_nombre, usl_apellido, usl_telefono FROM usl_usuarios_libros WHERE usl_codigo = :usl_codigo LIMIT 1';
		$obj = new Conexion();
		$datos = $obj->CargarDatos($sql, $parametros);

		return $datos;
	}
}
 ?>