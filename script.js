
    $(document).off("click",'.btnOperacion');
    $(document).on("click",'.btnOperacion', function(e){
        e.preventDefault();

        var operacion = $(this).attr('data-operacion');	// registrar, eliminar,  modificar o guardar cambios
        var destino = $(this).attr('data-destino');
        var formDestino = $(this).attr('data-destino-form');
        // alert('d '+return);
        var metodo = 'POST';
        var parametros = '';
        var puntos  = '../';

        var textoAlerta;
        if(operacion === "registrar"){
        	var form = $('.FormularioAjax');
            parametros =  form.serialize()
            textoAlerta = "Los datos quedarán almacenados en el sistema";
            textoResultado = 'Registro almacenado con éxito.';

        }else if(operacion === "eliminar"){
            var idRegistro = $(this).attr('id');
            parametros = {'codigo': idRegistro, 'op': 'eliminar', 'puntos': puntos};
            textoAlerta="Los datos serán eliminados completamente del sistema";
            textoResultado = 'Registro eliminado con éxito.';

        }else if(operacion === "modificar"){
            var idRegistro = $(this).attr('id');
            $.post(destino, { 'puntos' : puntos, 'codigo': idRegistro }, function(data){
                $('#cargarForm').html(data);
            });
            return;
        }else if(operacion === "guardarCambios"){
            var form = $('.FormularioAjax');
            parametros =  form.serialize()
            textoAlerta = "El registro será actualizado.";
            textoResultado = 'Registro actualizado con éxito.';
        }else if(operacion === "cancelar"){
            var idRegistro = $(this).attr('id');
            $.post(destino, { 'puntos' : puntos, 'codigo': idRegistro }, function(data){
                $('#cargarForm').html(data);
            });
            return;
        }else{
            textoAlerta="";
        }

        if (!confirm("¿ Está seguro de realizar esta operación ? " + textoAlerta)) {
        	return;
        }

        $.ajax({
        	url: destino,
        	type: metodo,
        	data: parametros,
        	// dataType: 'json',
        	cache: false,
        })
        .done(function(result) {
        	if (result == 1) {
                if (operacion == 'registrar') {
                    $.post(formDestino, {}, function(respuesta) {
                        $('#cargarTabla').html(respuesta);
                    });
                    $(form)[0].reset();
                    // $('#txtNombre').val('');
                    // $('#txtDescripcion').val('');
                }

                if (operacion  === 'eliminar') {
                    $("#fila" + idRegistro).remove();
                    // $('#msjRespuesta').html(textoResultado);
                }

                if (operacion  === 'guardarCambios') {
                    $.post(formDestino, {}, function(respuesta) {
                        $('#contenido').html(respuesta);
                    });
                    // $('#msjRespuesta').html(textoResultado);
                }

                $('#msjRespuesta').html(textoResultado);
                $("#divRespuesta").fadeTo(2000, 500).slideUp(500, function(){
                    $("#divRespuesta").slideUp(500);
                });
        	} else {
        		alert('Error inesperado, recargue la página y vuelva intentarlo.');
        	}
        })
        .fail(function() {
        	alert('Error inesperado al intentar comunicarse con el servidor.');
        })
    });