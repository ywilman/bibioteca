-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-11-2018 a las 03:54:23
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.0.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdbiblioteca`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aut_autores`
--

CREATE TABLE `aut_autores` (
  `aut_codigo` int(11) NOT NULL,
  `aut_nombre` varchar(30) NOT NULL,
  `aut_apellido` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `aut_autores`
--

INSERT INTO `aut_autores` (`aut_codigo`, `aut_nombre`, `aut_apellido`) VALUES
(1, 'abc', 'def'),
(10, 'viejo', 'sapo'),
(12, 'dsdgdf', 'd'),
(13, 'castro', 'mario'),
(14, 'editado', 'xcv'),
(15, 'ooo', 'df');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_categorias`
--

CREATE TABLE `cat_categorias` (
  `cat_codigo` int(11) NOT NULL,
  `cat_nombre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cat_categorias`
--

INSERT INTO `cat_categorias` (`cat_codigo`, `cat_nombre`) VALUES
(1, 'Cuentos'),
(2, 'ProgramaciÃ³n'),
(5, 'tambiÃ©n lo edite esta categoria');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detp_detalle_prestamo`
--

CREATE TABLE `detp_detalle_prestamo` (
  `detp_codigo` int(11) NOT NULL,
  `detp_codpre` int(11) NOT NULL,
  `detp_codlib` int(11) NOT NULL,
  `detp_cantidad` int(11) NOT NULL,
  `detp_fecha_prestamo` datetime NOT NULL,
  `detp_fecha_devolucion` datetime NOT NULL,
  `detp_fecha_real_devolucion` datetime NOT NULL,
  `detp_estado` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detp_detalle_prestamo`
--

INSERT INTO `detp_detalle_prestamo` (`detp_codigo`, `detp_codpre`, `detp_codlib`, `detp_cantidad`, `detp_fecha_prestamo`, `detp_fecha_devolucion`, `detp_fecha_real_devolucion`, `detp_estado`) VALUES
(104, 121, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Devolver'),
(124, 132, 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Devolver'),
(126, 133, 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Devolver'),
(127, 133, 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Devolver'),
(128, 135, 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Devolver');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `libd_libros_devueltos`
--

CREATE TABLE `libd_libros_devueltos` (
  `libd_codigo` int(11) NOT NULL,
  `libd_codpre` int(11) NOT NULL,
  `libd_codlib` int(11) NOT NULL,
  `libd_cantidad` int(11) NOT NULL,
  `libd_fecha_prestamo` datetime NOT NULL,
  `libd_fecha_devolucion` datetime NOT NULL,
  `libd_fecha_real_devolucion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `libd_libros_devueltos`
--

INSERT INTO `libd_libros_devueltos` (`libd_codigo`, `libd_codpre`, `libd_codlib`, `libd_cantidad`, `libd_fecha_prestamo`, `libd_fecha_devolucion`, `libd_fecha_real_devolucion`) VALUES
(3, 71, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 70, 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 76, 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 76, 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 77, 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 77, 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 78, 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 80, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 80, 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 79, 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 86, 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 81, 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 94, 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 90, 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 90, 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 90, 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 101, 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 101, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 101, 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 101, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 103, 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 103, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 103, 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 103, 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 105, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 108, 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 107, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 107, 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 110, 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 110, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 112, 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 112, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 118, 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 118, 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 122, 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 122, 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 123, 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 124, 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 125, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 126, 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 126, 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 127, 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 127, 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 119, 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 119, 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 128, 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 128, 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 131, 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 129, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lib_libros`
--

CREATE TABLE `lib_libros` (
  `lib_codigo` int(11) NOT NULL,
  `lib_nombre` varchar(30) NOT NULL,
  `lib_descripcion` varchar(250) NOT NULL,
  `lib_codaut` int(11) NOT NULL,
  `lib_codcat` int(11) NOT NULL,
  `lib_codedit` int(11) NOT NULL,
  `lib_isbn` varchar(100) NOT NULL,
  `lib_cant_ejemplares` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `lib_libros`
--

INSERT INTO `lib_libros` (`lib_codigo`, `lib_nombre`, `lib_descripcion`, `lib_codaut`, `lib_codcat`, `lib_codedit`, `lib_isbn`, `lib_cant_ejemplares`) VALUES
(1, 'Mio cid', 'El mio si', 14, 0, 0, '', 15),
(4, 'La alacrana', 'verde', 2, 0, 0, '', 20),
(5, 'El lado positivo del fracaso', 'ninguna', 13, 0, 0, '', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `par_parametros`
--

CREATE TABLE `par_parametros` (
  `par_codigo` int(11) NOT NULL,
  `par_num_maximo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `par_parametros`
--

INSERT INTO `par_parametros` (`par_codigo`, `par_num_maximo`) VALUES
(1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pre_prestamos`
--

CREATE TABLE `pre_prestamos` (
  `pre_codigo` int(11) NOT NULL,
  `pre_codusl` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pre_prestamos`
--

INSERT INTO `pre_prestamos` (`pre_codigo`, `pre_codusl`) VALUES
(70, 1),
(71, 3),
(75, 2),
(76, 2),
(77, 2),
(78, 1),
(79, 1),
(80, 2),
(81, 4),
(82, 2),
(83, 3),
(84, 1),
(85, 3),
(86, 1),
(87, 3),
(88, 1),
(89, 1),
(90, 1),
(91, 3),
(92, 3),
(93, 3),
(94, 4),
(95, 4),
(96, 4),
(97, 4),
(98, 4),
(99, 4),
(100, 4),
(101, 4),
(102, 1),
(103, 1),
(104, 4),
(105, 4),
(106, 1),
(107, 1),
(108, 4),
(109, 4),
(110, 1),
(111, 4),
(112, 1),
(113, 1),
(114, 4),
(115, 1),
(116, 2),
(117, 4),
(118, 1),
(119, 2),
(120, 3),
(121, 4),
(122, 1),
(123, 1),
(124, 1),
(125, 1),
(126, 1),
(127, 1),
(128, 1),
(129, 2),
(130, 1),
(131, 1),
(132, 3),
(133, 5),
(134, 1),
(135, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usl_usuarios_libros`
--

CREATE TABLE `usl_usuarios_libros` (
  `usl_codigo` int(11) NOT NULL,
  `usl_nombre` varchar(30) NOT NULL,
  `usl_apellido` varchar(30) NOT NULL,
  `usl_telefono` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usl_usuarios_libros`
--

INSERT INTO `usl_usuarios_libros` (`usl_codigo`, `usl_nombre`, `usl_apellido`, `usl_telefono`) VALUES
(1, 'Toche', 'Parche', '7589-9658'),
(2, 'Pedro', 'Castillo', '2569-9684'),
(3, 'Adolf', 'Hitler', '7589-9685'),
(4, 'Diego', 'Cruz', '7847-0594'),
(5, 'Juan', 'Castro', '7589-9685');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aut_autores`
--
ALTER TABLE `aut_autores`
  ADD PRIMARY KEY (`aut_codigo`);

--
-- Indices de la tabla `cat_categorias`
--
ALTER TABLE `cat_categorias`
  ADD PRIMARY KEY (`cat_codigo`);

--
-- Indices de la tabla `detp_detalle_prestamo`
--
ALTER TABLE `detp_detalle_prestamo`
  ADD PRIMARY KEY (`detp_codigo`);

--
-- Indices de la tabla `libd_libros_devueltos`
--
ALTER TABLE `libd_libros_devueltos`
  ADD PRIMARY KEY (`libd_codigo`);

--
-- Indices de la tabla `lib_libros`
--
ALTER TABLE `lib_libros`
  ADD PRIMARY KEY (`lib_codigo`);

--
-- Indices de la tabla `par_parametros`
--
ALTER TABLE `par_parametros`
  ADD PRIMARY KEY (`par_codigo`);

--
-- Indices de la tabla `pre_prestamos`
--
ALTER TABLE `pre_prestamos`
  ADD PRIMARY KEY (`pre_codigo`);

--
-- Indices de la tabla `usl_usuarios_libros`
--
ALTER TABLE `usl_usuarios_libros`
  ADD PRIMARY KEY (`usl_codigo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aut_autores`
--
ALTER TABLE `aut_autores`
  MODIFY `aut_codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `cat_categorias`
--
ALTER TABLE `cat_categorias`
  MODIFY `cat_codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `detp_detalle_prestamo`
--
ALTER TABLE `detp_detalle_prestamo`
  MODIFY `detp_codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT de la tabla `libd_libros_devueltos`
--
ALTER TABLE `libd_libros_devueltos`
  MODIFY `libd_codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT de la tabla `lib_libros`
--
ALTER TABLE `lib_libros`
  MODIFY `lib_codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `par_parametros`
--
ALTER TABLE `par_parametros`
  MODIFY `par_codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `pre_prestamos`
--
ALTER TABLE `pre_prestamos`
  MODIFY `pre_codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT de la tabla `usl_usuarios_libros`
--
ALTER TABLE `usl_usuarios_libros`
  MODIFY `usl_codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
